package main

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
)

func connect() (db *sql.DB, err error) {

	db, err = sql.Open("mysql", "root:0000@tcp(127.0.0.1:3306)/golang")

	if err != nil {
		panic(err.Error())
	}

	fmt.Println("Success!")

	return db, nil
}
