module todo

go 1.15

require (
	github.com/go-chi/chi/v5 v5.0.12
	github.com/go-sql-driver/mysql v1.8.0
	github.com/golang-jwt/jwt/v5 v5.2.1
)
