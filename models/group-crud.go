package models

import (
	"context"
	"log"
)

type Group struct {
	Id       int    `json:"id"`
	Title    string `json:"title"`
	AuthorId int    `json:"author_id"`
}

func (g *Group) GetAllGroups() ([]*Group, error) {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	stmt := `SELECT * FROM tasks_groups JOIN users ON tasks_groups.author_id=users.author_id LIMIT ? OFFSET ? ORDER BY id DESC `

	rows, err := db.QueryContext(ctx, stmt, 10, 0)
	if err != nil {
		log.Println(err)
	}
	defer rows.Close()

	var taskGroups []*Group
	for rows.Next() {
		var group Group
		err = rows.Scan(
			&group.Id,
			&group.Title,
			&group.AuthorId,
		)
		taskGroups = append(taskGroups, &group)
	}
	return taskGroups, nil
}

func (g *Group) GetGroupById(id int) (*Group, error) {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	stmt := `SELECT * FROM tasks_groups JOIN users ON tasks_groups.author_id=users.author_id WHERE id=?`

	row := db.QueryRowContext(ctx, stmt, id)
	var taskGroup Group
	err := row.Scan(
		&taskGroup.Id,
		&taskGroup.Title,
		&taskGroup.AuthorId,
	)
	if err != nil {
		return &Group{}, err
	}
	return &taskGroup, nil
}

func (g *Group) CreateGroup(user *User) error {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	stmt := `INSERT INTO tasks_groups (title, author_id) VALUES (?, ?)`

	err := db.QueryRowContext(ctx, stmt, g.Title, user.Id)
	if err != nil {
		return err.Err()
	}
	return nil
}

func (g *Group) UpdateGroup(user *User) error {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	stmt := `UPDATE tasks_groups SET title=? , author_id=? WHERE id=?`
	_, err := db.ExecContext(ctx, stmt, g.Title, user.Id, g.Id)
	if err != nil {
		return err
	}
	return nil
}

func (g *Group) DeleteGroup(id int) error {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	stmt := `DELETE FROM tasks_groups WHERE id=?`
	_, err := db.ExecContext(ctx, stmt, id)
	if err != nil {
		return err
	}
	return nil
}
