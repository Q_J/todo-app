package models

import (
	"context"
	"log"
)

type Task struct {
	Id       int    `json:"id"`
	Title    string `json:"title"`
	GroupId  int    `json:"group_id"`
	AuthorId int    `json:"author_id"`
}

func (t *Task) GetAllTasks() ([]*Task, error) {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	stmt := `SELECT * FROM tasks
         JOIN users 
             ON tasks.author_id=users.id
         JOIN task_groups 
             ON tasks.group_id=task_groups.id
         ORDER BY id DESC LIMIT ? OFFSET ?`

	rows, err := db.QueryContext(ctx, stmt, 10, 0)
	if err != nil {
		log.Panic(err)
	}
	defer rows.Close()

	var Tasks []*Task
	for rows.Next() {
		var task Task
		err := rows.Scan(
			&task.Id,
			&task.Title,
			&task.GroupId,
			&task.AuthorId,
		)
		if err != nil {
			return nil, err
		}
		Tasks = append(Tasks, &task)
	}
	return Tasks, nil
}

func (t *Task) GetTaskById(id int) (*Task, error) {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	stmt := `SELECT * FROM tasks
         JOIN users 
             ON tasks.author_id=users.id
         JOIN task_groups 
             ON tasks.group_id=task_groups.id
         WHERE id=?`

	var task Task
	row := db.QueryRowContext(ctx, stmt, id)
	err := row.Scan(
		&task.Id,
		&task.Title,
		&task.GroupId,
		&task.AuthorId,
	)
	if err != nil {
		return &Task{}, err
	}
	return &task, nil
}

func (t *Task) CreateTask(user User, group Group) error {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	stmt := `INSERT INTO tasks (title, group_id, author_id) VALUES (?, ?, ?)`

	err := db.QueryRowContext(ctx, stmt, t.Title, group.Id, user.Id)
	if err != nil {
		return err.Err()
	}
	return nil
}

func (t *Task) UpdateTask(user User, group Group) error {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	stmt := `UPDATE tasks SET title=?, group_id=?, author_id=? WHERE id=?`
	err := db.QueryRowContext(ctx, stmt, t.Title, group.Id, user.Id, t.Id)
	if err.Err() != nil {
		return err.Err()
	}
	return nil
}

func (t *Task) DeleteTask(id int) error {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	stmt := `DELETE FROM tasks WHERE id=?`
	_, err := db.ExecContext(ctx, stmt, id)
	if err != nil {
		return err
	}
	return nil
}
