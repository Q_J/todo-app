package models

import (
	"database/sql"
	"time"
)

type Models struct {
	Task  Task
	Group Group
	User  User
}

type ResponseJson struct {
	Error bool   `json:"error"`
	Data  string `json:"data"`
}

var db *sql.DB

const dbTimeout = time.Second * 5

func New(sql *sql.DB) Models {
	db = sql

	return Models{
		Task:  Task{},
		Group: Group{},
		User:  User{},
	}
}
