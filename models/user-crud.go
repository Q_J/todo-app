package models

import (
	"context"
	"log"
)

type User struct {
	Id       int    `json:"id"`
	Username string `json:"username"`
	Password string `json:"password"`
}

type CheckUser struct {
	UserExists int `json:"user_exists"`
}

func (u *User) GetAllUsers() ([]*User, error) {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	stmt := `SELECT id, username, password FROM users ORDER BY id DESC LIMIT ? OFFSET ?`

	rows, err := db.QueryContext(ctx, stmt, 10, 0)
	if err != nil {
		log.Panic(err)
	}
	defer rows.Close()

	var Users []*User

	for rows.Next() {
		var user User

		err := rows.Scan(
			&user.Id,
			&user.Username,
			&user.Password,
		)
		if err != nil {
			return nil, err
		}

		Users = append(Users, &user)
	}

	return Users, nil
}

func (u *User) GetUserById(id int) *User {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	stmt := `SELECT * FROM users WHERE id=? LIMIT 1`
	row := db.QueryRowContext(ctx, stmt, id)

	var user User

	err := row.Scan(
		&user.Id,
		&user.Username,
		&user.Password,
	)
	if err != nil {
		log.Panicln(err)
	}

	return &user
}

func (u *User) CreateUser(user User) error {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	stmt := `INSERT INTO users (username, password) VALUES (?, ?)`
	err := db.QueryRowContext(ctx, stmt, u.Username, u.Password)
	if err != nil {
		return err.Err()
	}

	return nil
}

func (u *User) UpdateUser(payload User) error {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	stmt := `UPDATE users SET username=? WHERE id=?`

	_, err := db.ExecContext(ctx, stmt, payload.Username, payload.Id)

	if err != nil {
		return err
	}

	return nil
}

func (u *User) RemoveUser() error {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	stmt := `DELETE FROM users WHERE id=?`

	_, err := db.ExecContext(ctx, stmt, u.Id)

	if err != nil {
		return err
	}

	return nil
}

// For log-in
func (u *User) GetUserByUsername(username string) *User {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	stmt := `SELECT id FROM users WHERE username=? LIMIT 1`
	row := db.QueryRowContext(ctx, stmt, username)

	var user User

	err := row.Scan(
		&user.Id,
	)
	if err != nil {
		return &User{}
	}

	return &user
}

func (u *User) CheckUserExistence(user *User) bool {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	stmt := `SELECT COUNT(*) FROM users WHERE username=? AND password=?`

	row, err := db.QueryContext(ctx, stmt, user.Username, user.Password)
	if err != nil {
		panic(err)
	}

	var result CheckUser

	for row.Next() {
		err := row.Scan(&result.UserExists)
		if err != nil {
			log.Println(err)
			return false
		}
	}

	return result.UserExists != 0
}
