package main

import (
	"database/sql"
	"log"
	"net/http"
	"todo/models"

	_ "github.com/go-sql-driver/mysql"
)

type Config struct {
	Db     *sql.DB
	Models models.Models
}

func main() {
	db, err := connect()
	if err != nil {
		log.Panicln("Unable to connect MySQL: ", err)
	}
	err = db.Ping()
	if err != nil {
		log.Panicln("Unable to ping MySQL: ", err)
	}

	config := Config{
		Db:     db,
		Models: models.New(db),
	}
	server := &http.Server{
		Addr:    ":8080",
		Handler: config.router(),
	}
	server.ListenAndServe()
}
