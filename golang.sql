CREATE DATABASE golang;

USE golang;

CREATE TABLE IF NOT EXISTS users (
    id          INT AUTO_INCREMENT PRIMARY KEY,
    username    VARCHAR(100) UNIQUE,
    password    VARCHAR(300) NOT NULL
);

CREATE TABLE IF NOT EXISTS tasks (
    id          INT AUTO_INCREMENT PRIMARY KEY,
    title       VARCHAR(300),
    author_id   INT NOT NULL,
    group_id    INT NOT NULL,
    FOREIGN KEY (author_id) REFERENCES users (id),
    FOREIGN KEY (group_id) REFERENCES tasks_groups (id)
);

CREATE TABLE IF NOT EXISTS tasks_groups (
    id          INT AUTO_INCREMENT PRIMARY KEY,
    title       VARCHAR(300),
    author_id     INT NOT NULL,
    FOREIGN KEY (author_id) REFERENCES users (id)
);

