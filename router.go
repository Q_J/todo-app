package main

import (
	"encoding/json"
	"github.com/go-chi/chi/v5"
	"log"
	"net/http"
)

type Response struct {
	Status string `json:"status"`
	Data   string `json:"data"`
}

func (config *Config) router() http.Handler {
	mux := chi.NewRouter()

	data := Response{
		Status: "Status ok",
		Data:   "This is valid data for this test",
	}

	ret, err := json.Marshal(data)

	if err != nil {
		log.Panicln(err)
	}

	mux.Get("/", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		_, err := w.Write(ret)

		if err != nil {
			log.Panicln(err)
		}
	})

	mux.Get("/users", AuthMiddleware(config.HandlerGetAllUsers))
	mux.Get("/users/{id}", AuthMiddleware(config.HandlerGetUserById))
	mux.Put("/users", AuthMiddleware(config.HandlerUpdateUser))
	mux.Delete("/users/{id}", AuthMiddleware(config.HandlerRemoveUser))

	// mux.Get("/groups", config.HandlerGetAllUsers)
	// mux.Get("/groups/{id}", config.HandlerGetUserById)
	// mux.Post("/groups", config.HandlerCreateUser)
	// mux.Put("/groups", nil)
	// mux.Delete("/groups/{id}", config.HandlerRemoveUser)

	// mux.Get("/users", config.HandlerGetAllUsers)
	// mux.Get("/users/{id}", config.HandlerGetUserById)
	// mux.Post("/users", config.HandlerCreateUser)
	// mux.Put("/users", nil)
	// mux.Delete("/users/{id}", config.HandlerRemoveUser)

	// Used for login
	mux.Post("/login", config.LoginUser)
	// Used for registration
	mux.Post("/users", config.HandlerCreateUser)

	return mux
}
