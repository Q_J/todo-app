package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"time"
	"todo/models"

	"github.com/golang-jwt/jwt/v5"
)

type TokenClaims struct {
	Sub string `json:"sub"`
	Exp int64  `json:"exp"`
}

var (
	key []byte
	t   *jwt.Token
	s   string
)

func createToken(claims *TokenClaims) (string, error) {
	var (
		t *jwt.Token
		s string
	)
	claims.Exp = time.Now().Add(time.Hour * 24).Unix()
	key = []byte("L#5Ab^yFVNM!MA|}s+qQFN.TI6'Rc3#")
	t = jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"sub": claims.Sub,
		"exp": claims.Exp,
	})

	s, err := t.SignedString(key)
	if err != nil {
		return "", err
	}
	return s, nil
}

func verifyToken(tokenSting string) error {
	token, err := jwt.Parse(tokenSting, func(t *jwt.Token) (interface{}, error) {
		return key, nil
	})
	if err != nil {
		log.Println(err)
		return err
	}

	if !token.Valid {
		return errors.New("Invalid token!")
	}
	return nil
}

func (c *Config) LoginUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var u models.User

	json.NewDecoder(r.Body).Decode(&u)

	exists := u.CheckUserExistence(&u)

	fmt.Println(exists)
}

// AuthMiddleware provider to JWT
func AuthMiddleware(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		authToken := r.Header.Get("Authorization")
		if authToken == "" {
			http.Error(w, "Unauthorized", http.StatusUnauthorized)
			return
		}
		next.ServeHTTP(w, r)
	}
}
