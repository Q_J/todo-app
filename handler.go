package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"path"
	"strconv"
	"todo/models"
)

type Credentials struct {
	Username string
	Password string
}

func (config *Config) HandlerGetAllUsers(w http.ResponseWriter, r *http.Request) {
	u := models.User{}

	users, err := u.GetAllUsers()

	if err != nil {
		log.Panicln(err)
	}

	out, err := json.Marshal(users)
	if err != nil {
		log.Panicln(err)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(out)
}

func (config *Config) HandlerGetUserById(w http.ResponseWriter, r *http.Request) {
	u := models.User{}

	id, err := strconv.Atoi(path.Base(r.RequestURI))
	if err != nil {
		log.Panicln(err)
	}

	users := u.GetUserById(id)

	if err != nil {
		log.Panicln(err)
	}

	out, err := json.Marshal(users)
	if err != nil {
		log.Panicln(err)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(out)
}

func (config *Config) HandlerCreateUser(w http.ResponseWriter, r *http.Request) {
	u := models.User{}

	err := json.NewDecoder(r.Body).Decode(&u)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	fmt.Println(u)

	err = u.CreateUser(u)
	if err != nil {
		log.Panicln(err)
	}
	fmt.Println("New user created!")
}

func (config *Config) HandlerUpdateUser(w http.ResponseWriter, r *http.Request) {
	u := models.User{}

	err := json.NewDecoder(r.Body).Decode(&u)

	res := models.ResponseJson{
		Error: false,
		Data:  "",
	}
	if err != nil {
		res.Error = true
		log.Panicln(err)
	}

	err = config.Models.User.UpdateUser(u)
	if err != nil {
		res.Error = true
		res.Data = fmt.Sprintf("Error while updating the user: %s", err)
	} else {
		res.Data = fmt.Sprintln("User data updated successfully")
	}

	j, err := json.Marshal(res)
	if err != nil {
		log.Panicln("JSON error: ", err)
	}

	w.WriteHeader(http.StatusAccepted)
	w.Write(j)
}

func (config *Config) HandlerRemoveUser(w http.ResponseWriter, r *http.Request) {
	u := models.User{}

	id, err := strconv.Atoi(path.Base(r.RequestURI))
	if err != nil {
		log.Panicln(err)
	}

	user := u.GetUserById(id)
	if user != nil {
		user.RemoveUser()
		fmt.Println("User succesfully removed!")
	} else {
		fmt.Println("User not found!")
	}
}

//func (config *Config) LoginUser(w http.ResponseWriter, r *http.Request) {
//	w.Header().Set("Content-Type", "application/json")
//	c := Credentials{}
//
//	err := json.NewDecoder(r.Body).Decode(&c)
//	if err != nil {
//		log.Panic(err)
//	}
//	u := config.Models.User.CheckUserExistence(&models.User{})
//	fmt.Println(u)

// if u.Id != 0 {
// 	res := models.ResponseJson{
// 		Error: false,
// 		Data:  "Welcome!",
// 	}
// 	r, _ := json.Marshal(res)

// 	cookie := http.Cookie{
// 		Name:  "Authorization",
// 		Value: "ok",
// 	}
// 	http.SetCookie(w, &cookie)
// 	w.Write(r)
// } else {
// 	res := models.ResponseJson{
// 		Error: true,
// 		Data:  "Credentials is not correct!",
// 	}
// 	r, _ := json.Marshal(res)
// 	w.Write(r)
// }
//}
